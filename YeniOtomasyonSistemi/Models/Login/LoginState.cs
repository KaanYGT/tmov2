﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace YeniOtomasyonSistemi.Models.Login
{
    public class LoginState
    {
        public LoginState()
        {
            LidosSistemiEntities db = new LidosSistemiEntities();
        }

        public bool IsLoginSuccess(string user,string pwd)
        {
            LidosSistemiEntities db = new LidosSistemiEntities();
            LKullanicilar resultUser = db.LKullanicilar.Where(x => x.KullaniciAdi.Trim().ToString().Equals(user) && x.KullaniciSifresi.Equals(pwd)).FirstOrDefault();
            if (resultUser != null)
            {
                LSubeler SubeBilgisi = resultUser.LSubeler;
                HttpContext.Current.Session.Add("KullaniciId", resultUser.KullaniciId);
                HttpContext.Current.Session.Add("KullaniciAdi", resultUser.KullaniciAdi);
                HttpContext.Current.Session.Add("KullaniciSubeId", resultUser.KullaniciSubesi);
                HttpContext.Current.Session.Add("KullaniciSubeKodu", SubeBilgisi.SubeKisaKodu);
                return true;
            }
            return new bool();
        }
    }
}