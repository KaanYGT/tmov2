﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace YeniOtomasyonSistemi.Models.Login
{
    public class ControlLogin:ActionFilterAttribute,IActionFilter
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            try
            {
                if (!string.IsNullOrEmpty(HttpContext.Current.Session["KullaniciId"].ToString()))
                {
                    base.OnActionExecuting(filterContext);
                }
                else
                {
                    HttpContext.Current.Response.Redirect("/Security/Login");
                }
            }
            catch (Exception)
            {
                HttpContext.Current.Response.Redirect("/Security/Login");
            }
        }
    }
}