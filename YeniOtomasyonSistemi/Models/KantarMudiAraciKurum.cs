//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace YeniOtomasyonSistemi.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class KantarMudiAraciKurum
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public KantarMudiAraciKurum()
        {
            this.KantarMudiBilgileri = new HashSet<KantarMudiBilgileri>();
        }
    
        public int AraciKurumId { get; set; }
        public string AraciKurumAdi { get; set; }
        public string AraciKurumKodu { get; set; }
        public string AraciKurumTuru { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<KantarMudiBilgileri> KantarMudiBilgileri { get; set; }
    }
}
