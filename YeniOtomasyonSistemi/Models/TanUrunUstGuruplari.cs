//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace YeniOtomasyonSistemi.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class TanUrunUstGuruplari
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TanUrunUstGuruplari()
        {
            this.LabLaboratuvarAnalizListesi = new HashSet<LabLaboratuvarAnalizListesi>();
            this.TanDepoTanimlamalari = new HashSet<TanDepoTanimlamalari>();
            this.TanUrunGuruplari = new HashSet<TanUrunGuruplari>();
        }
    
        public int UrunUstGurupKoduId { get; set; }
        public string UrunUstGurupKodu { get; set; }
        public string UrunUstGurupAdi { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LabLaboratuvarAnalizListesi> LabLaboratuvarAnalizListesi { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TanDepoTanimlamalari> TanDepoTanimlamalari { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TanUrunGuruplari> TanUrunGuruplari { get; set; }
    }
}
