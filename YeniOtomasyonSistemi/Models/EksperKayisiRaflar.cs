//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace YeniOtomasyonSistemi.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class EksperKayisiRaflar
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public EksperKayisiRaflar()
        {
            this.EksperKayisiKasalar = new HashSet<EksperKayisiKasalar>();
        }
    
        public int RafId { get; set; }
        public string RafAdi { get; set; }
        public Nullable<int> RafKapasitesi { get; set; }
        public Nullable<int> RafDoluluk { get; set; }
        public Nullable<int> RafBosAlan { get; set; }
        public Nullable<int> RafKatSayisi { get; set; }
        public Nullable<int> RafBolumSayisi { get; set; }
        public Nullable<int> HerBolumdekiKasaSayisi { get; set; }
        public Nullable<int> RafinBulunduguDepoId { get; set; }
        public Nullable<int> RaftaBulunanUrunId { get; set; }
        public string SubeKodu { get; set; }
    
        public virtual EksperKayisiDepolar EksperKayisiDepolar { get; set; }
        public virtual TanUrunler TanUrunler { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EksperKayisiKasalar> EksperKayisiKasalar { get; set; }
    }
}
