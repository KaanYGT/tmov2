﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace YeniOtomasyonSistemi.Models
{
    public static class KayisiLaboratuvarMetodlari
    {
        public class AnalizKalemleri
        {
            public int TaneAdeti { get; set; }
            public decimal RutubetOrani { get; set; }
            public decimal AgirKirliTane { get; set; }
            public decimal HafifKirliTane { get; set; }
            public decimal HasarliTane { get; set; }
            public decimal CilliTane { get; set; }
            public decimal DoluYaraliTane { get; set; }
            public decimal GunesYanigi { get; set; }
            public decimal RenkUyumsuzlugu { get; set; }
            public decimal CekirdekliKayisi { get; set; }
            public decimal BocekHasarli { get; set; }
            public decimal OluBocekParcalari { get; set; }
            public decimal YabanciMaddeBitkisel { get; set; }
            public decimal YabanciMaddeTehlikeli { get; set; }
            public int KukurtOrani { get; set; }
            public int AnalizListesiId { get; set; }

        }
        public class AnalizSonuclari
        {
            public int BoyDerecesi { get; set; }
            public string KayisiKukurtTipi { get; set; }
            public decimal ToplamKusur { get; set; }
            public string RedEdilmeNedeni { get; set; }
            public string GurupAdi { get; set; }
           

        }
        public class AnalizSonucuCikanUrunBilgileri
        {
            public string ISINKODU { get; set; }
            public string UrunKodu { get; set; }
            public string UrunAdi { get; set; }
        }
        public static int BoyDerecesiBul(int TaneAdeti)
        {
            int Boyu = 0;
            if (TaneAdeti>=0&&TaneAdeti<=80)
            {
                Boyu = 0;
            }
            else if (TaneAdeti >= 81 && TaneAdeti <= 100)
            {
                Boyu = 1;
            }
            else if (TaneAdeti >= 101 && TaneAdeti <= 120)
            {
                Boyu = 2;
            }
            else if (TaneAdeti >= 121 && TaneAdeti <= 140)
            {
                Boyu = 3;
            }
            else if (TaneAdeti >= 141 && TaneAdeti <= 160)
            {
                Boyu = 4;
            }
            else if (TaneAdeti >= 161 && TaneAdeti <= 180)
            {
                Boyu = 5;
            }
            else if (TaneAdeti >= 181 && TaneAdeti <= 200)
            {
                Boyu = 6;
            }
            else if (TaneAdeti >= 221 )
            {
                Boyu = 8;
            }
            return Boyu;
        }
        public static string KukurtTipiBul(int KukurtOrani)
        {
            string Tip = "";
            if (KukurtOrani>=2000&&KukurtOrani<=2500)
            {
                Tip = "TİP C";
            }
            else if (KukurtOrani >= 2501 && KukurtOrani <= 3000)
            {
                Tip = "TİP B";
            }
            else if (KukurtOrani >= 3001 && KukurtOrani <= 3500)
            {
                Tip = "TİP A" ;
            }
            else if (KukurtOrani > 3500)
            {
                Tip = "TİP D";
            }
            else if(KukurtOrani==0)
            {
                Tip = "KÜKÜRTLENMEMİŞ KURU KAYISI ";
            }
            return Tip;
        }
        public static decimal KusurOrani(AnalizKalemleri ak)
        {
            decimal ToplamKusur = 0;
            ToplamKusur = ToplamKusur + ak.AgirKirliTane;
            ToplamKusur = ToplamKusur + ak.HafifKirliTane;
            ToplamKusur = ToplamKusur + ak.HasarliTane;
            ToplamKusur = ToplamKusur + ak.CilliTane;
            ToplamKusur = ToplamKusur + ak.DoluYaraliTane;
            ToplamKusur = ToplamKusur + ak.GunesYanigi;
            ToplamKusur = ToplamKusur + ak.RenkUyumsuzlugu;
            ToplamKusur = ToplamKusur + ak.CekirdekliKayisi;
            ToplamKusur = ToplamKusur + ak.BocekHasarli;
            ToplamKusur = ToplamKusur + ak.OluBocekParcalari;
            ToplamKusur = ToplamKusur + ak.YabanciMaddeBitkisel;
            ToplamKusur = ToplamKusur + ak.YabanciMaddeTehlikeli;
            return ToplamKusur;
        }
        public static string RedEdilmeVarmi(AnalizKalemleri ak)
        {
            string RedEdilmeNedeni = "";
            decimal ToplamKusurOrani = KusurOrani(ak);
            if (ak.CekirdekliKayisi>1)
            {
                RedEdilmeNedeni = "Çekirdekli Kayısı Oranı %"+ak.CekirdekliKayisi.ToString()+" olduğundan iş yeri tarafından depolanması mümkün değildir.";
            }
            else if (ak.OluBocekParcalari > 1)
            {
                RedEdilmeNedeni = "Ölü böcek parçaları oranı %" + ak.OluBocekParcalari.ToString() + " olduğundan iş yeri tarafından depolanması mümkün değildir.";
            }
            else if (ak.YabanciMaddeBitkisel > Convert.ToDecimal(0.2))
            {
                RedEdilmeNedeni = "Bitkisel yabancı madde oranı %" + ak.YabanciMaddeBitkisel.ToString() + " olduğundan iş yeri tarafından depolanması mümkün değildir.";
            }
            else if (ak.YabanciMaddeTehlikeli > Convert.ToDecimal(0.2))
            {
                RedEdilmeNedeni = "Tehlikeli yabancı madde oranı %" + ak.YabanciMaddeTehlikeli.ToString() + " olduğundan iş yeri tarafından depolanması mümkün değildir.";
            }
            else if (ToplamKusurOrani > 20)
            {
                RedEdilmeNedeni = "Toplam Kusur Oranı %" + ToplamKusurOrani.ToString() + " olduğundan iş yeri tarafından depolanması mümkün değildir.";
            }
            else if (ak.AgirKirliTane > 5 ||
                ak.HafifKirliTane > 10 ||
                ak.HasarliTane > 8 ||
                ak.CilliTane > 5 ||
                ak.DoluYaraliTane > 5 ||
                ak.GunesYanigi > 3 ||
                ak.RenkUyumsuzlugu > 5 ||
                ak.BocekHasarli > 3)
            {
                RedEdilmeNedeni = "Girilen Değerler alım şartlarının üstünde olduğundan iş yeri tarafından depolanması mümkün değildir."; 
            }
            else if (ak.KukurtOrani > 0&&ak.KukurtOrani<=1999)
            {
                RedEdilmeNedeni = "2000 ppm altında kalan kükürtlenmiş kayısılar depolama için uygun değildir. Kükürt oranınız : " + ak.KukurtOrani + " olduğundan iş yeri tarafından depolanması mümkün değildir.";
            }
            else if (ak.KukurtOrani == 0 && ak.RutubetOrani >17)
            {
                RedEdilmeNedeni = "Yüksek Rutubet Oranı. Yapılan analizde  %" + ak.RutubetOrani + " rutubet olduğundan iş yeri tarafından depolanması mümkün değildir.";
            }
            else if (ak.KukurtOrani != 0 && ak.RutubetOrani > 18)
            {
                RedEdilmeNedeni = "Yüksek Rutubet Oranı. Yapılan analizde  %" + ak.RutubetOrani + " rutubet olduğundan iş yeri tarafından depolanması mümkün değildir.";
            }
            else
            {
                RedEdilmeNedeni = "Hayır";
            }
            return RedEdilmeNedeni;
        }

        public static string Gruplama(AnalizKalemleri ak)
        {
            string GrupAdi = "";
            
            if (ak.AgirKirliTane >= Convert.ToDecimal(3.01) && ak.AgirKirliTane <= 5 ||
                ak.HafifKirliTane >= Convert.ToDecimal(8.01) && ak.HafifKirliTane <= 10 ||
                ak.HasarliTane >= Convert.ToDecimal(6.01) && ak.HasarliTane <= 8 ||
                ak.CilliTane >= Convert.ToDecimal(3.01) && ak.CilliTane <= 5 ||
                ak.DoluYaraliTane >= Convert.ToDecimal(3.01) && ak.DoluYaraliTane <= 5 ||
                ak.GunesYanigi >= Convert.ToDecimal(2.01) && ak.GunesYanigi <= 3 ||
                ak.RenkUyumsuzlugu >= Convert.ToDecimal(3.01) && ak.RenkUyumsuzlugu <= 5 ||
                ak.BocekHasarli >= Convert.ToDecimal(2.01) && ak.BocekHasarli <= 3)
            {
                GrupAdi = "Gurup 3";
            }
            else if (ak.AgirKirliTane >= Convert.ToDecimal(1.01) && ak.AgirKirliTane <= 3 ||
                ak.HafifKirliTane >= Convert.ToDecimal(5.01) && ak.HafifKirliTane <= 8 ||
                ak.HasarliTane >= Convert.ToDecimal(3.01) && ak.HasarliTane <= 6 ||
                ak.CilliTane >= Convert.ToDecimal(1.01) && ak.CilliTane <= 3 ||
                ak.DoluYaraliTane >= Convert.ToDecimal(1.01) && ak.DoluYaraliTane <= 3 ||
                ak.GunesYanigi >= Convert.ToDecimal(1.01) && ak.GunesYanigi <= 2 ||
                ak.RenkUyumsuzlugu >= Convert.ToDecimal(1.01) && ak.RenkUyumsuzlugu <= 3 ||
                ak.BocekHasarli >= Convert.ToDecimal(1.01) && ak.BocekHasarli <= 2)
            {
                GrupAdi = "Gurup 2";
            }
            else if (ak.AgirKirliTane <= 1 ||
                ak.HafifKirliTane <= 5 ||
                ak.HasarliTane <= 3 ||
                ak.CilliTane <= 1 ||
                ak.DoluYaraliTane <= 1 ||
                ak.GunesYanigi <= 1 ||
                ak.RenkUyumsuzlugu <= 1 ||
                ak.BocekHasarli <= 1)
            {
                GrupAdi = "Gurup 1";
            }
            else
            {
                GrupAdi = "Red";
            }
            return GrupAdi;
        }
        public static AnalizSonuclari analizSonuclari(AnalizKalemleri analizKalemleri)
        {
            AnalizSonuclari AnS = new AnalizSonuclari();
            int Boyu = BoyDerecesiBul(analizKalemleri.TaneAdeti);
            string Tipi = KukurtTipiBul(analizKalemleri.KukurtOrani);
            decimal ToplamKusur = KusurOrani(analizKalemleri);
            string RedNedeni = RedEdilmeVarmi(analizKalemleri);
            string GrupAdi = Gruplama(analizKalemleri);
            AnS.BoyDerecesi = Boyu;
            AnS.GurupAdi = GrupAdi;
            AnS.KayisiKukurtTipi = Tipi;
            AnS.RedEdilmeNedeni = RedNedeni;
            AnS.ToplamKusur = ToplamKusur;
            return AnS;


        }
        public static AnalizSonucuCikanUrunBilgileri AnalizSonucuCikanUrun(AnalizSonuclari analiz)
        {
            AnalizSonucuCikanUrunBilgileri AnS = new AnalizSonucuCikanUrunBilgileri();
            LidosSistemiEntities db = new LidosSistemiEntities();
            int SubeId = Convert.ToInt32(HttpContext.Current.Session["KullaniciSubeId"].ToString());
            var UrunGetir = db.TanISINKodlari.Where(t => t.SubeId == SubeId &&
            t.TanUrunler.UrunDerecesi == analiz.BoyDerecesi.ToString() && t.TanUrunler.UrunAdi.Contains(analiz.KayisiKukurtTipi)).FirstOrDefault();

            AnS.ISINKODU = UrunGetir.ISINKodu;
            AnS.UrunKodu = UrunGetir.TanUrunler.UrunKodu;
            AnS.UrunAdi = UrunGetir.TanUrunler.UrunAdi;
            return AnS;
        }
    }
}