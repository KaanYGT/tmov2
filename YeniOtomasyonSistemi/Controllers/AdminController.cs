﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using YeniOtomasyonSistemi.Models;
using YeniOtomasyonSistemi.Models.Login;

namespace YeniOtomasyonSistemi.Controllers
{
    [ControlLogin]
    public class AdminController : Controller
    {
        public int UrunAdi { get; private set; }

        // GET: Admin
        [Authorize(Roles = "AnaMenuListesi")]
        public ActionResult AnaMenuListesi()
        {
            LidosSistemiEntities db = new LidosSistemiEntities();
            var sorgu2 = db.LMenuler.ToList();
            return View(sorgu2);
        }


        [HttpPost]
        [Authorize(Roles = "AnaMenuListesi")]
        public ActionResult AnaMenuEkle(string AnaMenuAdi, string IconAdi)
        {
            LidosSistemiEntities db = new LidosSistemiEntities();
            LMenuler lm = new LMenuler();
            lm.AnaMenuAdi = AnaMenuAdi;
            lm.IconAdi = IconAdi;
            db.LMenuler.Add(lm);
            db.SaveChanges();
            return RedirectToAction("AnaMenuListesi", "Admin");
        }

        [Authorize(Roles = "AltMenuListesi")]
        public ActionResult AltMenuListesi()
        {
            LidosSistemiEntities db = new LidosSistemiEntities();
            var sorgu2 = db.LMenuAlt.ToList();
            var sorgu3 = db.LMenuler.ToList();
            return View(Tuple.Create(sorgu2, sorgu3));

        }


        [Authorize(Roles = "AltMenuListesi")]
        [HttpPost]
        public ActionResult AltMenuEkle(string AnaMenuAdi, string AltMenuAdi, string ControllerAdi, string ActionAdi)
        {
            LidosSistemiEntities db = new LidosSistemiEntities();
            int AnaMenuId = db.LMenuler.Where(t => t.AnaMenuAdi == AnaMenuAdi).Select(t => t.AnaMenuId).FirstOrDefault();
            LMenuAlt lm = new LMenuAlt();
            lm.ActionAdi = ActionAdi;
            lm.AltMenuAdi = AltMenuAdi;
            lm.AnaMenuId = AnaMenuId;
            lm.ControllerAdi = ControllerAdi;
            db.LMenuAlt.Add(lm);
            db.SaveChanges();
            return RedirectToAction("AltMenuListesi", "Admin");
        }

        [Authorize(Roles = "GorevliListesi")]
        public ActionResult GorevliListesi()
        {
            LidosSistemiEntities db = new LidosSistemiEntities();
            var Gorevliler = db.TanGorevliler.ToList();
            var Subeler = db.LSubeler.ToList();
            return View(Tuple.Create(Gorevliler, Subeler));

        }
        [Authorize(Roles = "GorevliListesi")]
        [HttpPost]
        public ActionResult GorevliEkle(string GorevliAdiSoyadi,string GorevUnvani,string GorevBirimi,string SubeAdi)
        {
            LidosSistemiEntities db = new LidosSistemiEntities();
            int SubeId = db.LSubeler.Where(t => t.SubeAdi == SubeAdi).Select(t => t.SubeId).FirstOrDefault();
            string KullaniciAdi = Session["KullaniciAdi"].ToString();
            TanGorevliler gr = new TanGorevliler();
            gr.GorevBirimi = GorevBirimi;
            gr.GorevliAdiSoyadi = GorevliAdiSoyadi;
            gr.GorevliSubeId = SubeId;
            gr.GorevliUnvani = GorevUnvani;
            gr.KaydedenUser = KullaniciAdi;
            gr.KayitZamani = DateTime.Now;
            db.TanGorevliler.Add(gr);
            db.SaveChanges();
            return RedirectToAction("GorevliListesi");

        }
        [Authorize(Roles = "KullaniciListesi")]
        public ActionResult KullaniciListesi()
        {
            LidosSistemiEntities db = new LidosSistemiEntities();
            var Kullanicilar = db.LKullanicilar.ToList();
            var Subeler = db.LSubeler.ToList();
            return View(Tuple.Create(Kullanicilar, Subeler));

        }
        [Authorize(Roles = "KullaniciListesi")]
        [HttpPost]
        public ActionResult KullaniciEkle(string KullaniciAdi, string KullaniciSifresi, string KullaniciRolu, string SubeAdi)
        {
            LidosSistemiEntities db = new LidosSistemiEntities();
            var SubeId = db.LSubeler.Where(t => t.SubeAdi == SubeAdi).Select(t => t.SubeId).FirstOrDefault();
            LKullanicilar kl = new LKullanicilar();
            kl.KullaniciAdi = KullaniciAdi;
            kl.KullaniciRolu = KullaniciRolu;
            kl.KullaniciSifresi = KullaniciSifresi;
            kl.KullaniciSubesi = SubeId;
            db.LKullanicilar.Add(kl);
            db.SaveChanges();
            return RedirectToAction("KullaniciListesi", "Admin");

        }
        [Authorize(Roles = "KullaniciListesi")]
        public ActionResult KullaniciGuncelle(int? id)
        {
            LidosSistemiEntities db = new LidosSistemiEntities();
            var Kullanicilar = db.LKullanicilar.Where(t => t.KullaniciId == id).FirstOrDefault();
            var Subeler = db.LSubeler.ToList();
            return View(Tuple.Create(Kullanicilar, Subeler));

        }
        [Authorize(Roles = "KullaniciListesi")]
        [HttpPost]
        public ActionResult KullaniciGuncelle(int? id, string KullaniciSubesi, string KullaniciRolu, string KullaniciSifresi, string KullaniciAdi)
        {
            LidosSistemiEntities db = new LidosSistemiEntities();
            var kl = db.LKullanicilar.Where(t => t.KullaniciId == id).FirstOrDefault();
            var SubeId = db.LSubeler.Where(t => t.SubeAdi == KullaniciSubesi).Select(t => t.SubeId).FirstOrDefault();
            kl.KullaniciAdi = KullaniciAdi;
            kl.KullaniciRolu = KullaniciRolu;
            kl.KullaniciSifresi = KullaniciSifresi;
            kl.KullaniciSubesi = SubeId;
            db.SaveChanges();
            return RedirectToAction("KullaniciListesi", "Admin");

        }
        [Authorize(Roles = "KullaniciListesi")]
        public ActionResult KullaniciMenuleri(int id)
        {
            LidosSistemiEntities db = new LidosSistemiEntities();
            var AnaMenuler = db.LMenuler.ToList();
            var KullaniciAltMenuler = db.LKullaniciMenuler.Where(t => t.KullaniciId == id).Select(t => t.LMenuAlt).ToList();
            var TumAltMenuler = db.LMenuAlt.ToList();
            var KullaniciGetir = db.LKullanicilar.Where(t => t.KullaniciId == id).FirstOrDefault();
            return View(Tuple.Create(AnaMenuler, KullaniciAltMenuler,TumAltMenuler,KullaniciGetir));

        }
        [Authorize(Roles = "KullaniciListesi")]
        public ActionResult KullaniciMenuGuncelle(string[] isim, int UserId)
        {
            LidosSistemiEntities db = new LidosSistemiEntities();
            var MevcutDurum = db.LKullaniciMenuler.Where(t => t.KullaniciId == UserId).ToList();
            //Listede olmayan elemanları tespit edip databaseye ekliyoruz
            for (int i = 0; i < isim.Length; i++)
            {
                int KontrolEdilecekAltMenuId = Convert.ToInt32(isim[i]);
                var Kontrolet = MevcutDurum.Where(t => t.AltMenuId == KontrolEdilecekAltMenuId).FirstOrDefault();
                if (Kontrolet == null)
                {
                    LKullaniciMenuler km = new LKullaniciMenuler();
                    km.AltMenuId = KontrolEdilecekAltMenuId;
                    km.KullaniciId = UserId;
                    db.LKullaniciMenuler.Add(km);
                    db.SaveChanges();
                }

            }
            // Database üzerinden gelen elemanlar bizim listemiz içerisinde varsa duruyor fakat yoksa siliyoruz.
            var MevcutDurum2 = db.LKullaniciMenuler.Where(t => t.KullaniciId == UserId).ToList();
            for (int i = 0; i < MevcutDurum2.Count; i++)
            {
                if (isim.Contains(MevcutDurum2[i].AltMenuId.ToString()))
                {

                }
                else
                {
                    db.LKullaniciMenuler.Remove(MevcutDurum2[i]);
                    db.SaveChanges();
                }
            }
            
            return RedirectToAction("KullaniciListesi", "Admin");

        }


        #region IsinTanimlamalari
        [Authorize(Roles = "IsinKoduTanimlamalari")]
        public ActionResult IsinKoduTanimlamalari()
        {
            LidosSistemiEntities db = new LidosSistemiEntities();
            int SubeId = Convert.ToInt32(Session["KullaniciSubeId"].ToString());
            var TanimliIsinKodlari = db.TanISINKodlari.Where(t => t.SubeId == SubeId).ToList();
            var UrunGuruplari = db.TanUrunGuruplari.ToList();
       
            return View(Tuple.Create(TanimliIsinKodlari, UrunGuruplari));

        }
        [Authorize(Roles = "IsinKoduTanimlamalari")]
        public ActionResult ISINKaydet(int UrunId,string HasatDonemi,string ISINKodu)
        {
            LidosSistemiEntities db = new LidosSistemiEntities();
            int SubeId = Convert.ToInt32(Session["KullaniciSubeId"].ToString());
           
            TanISINKodlari tn = new TanISINKodlari();
            tn.HasatDonemi = HasatDonemi;
            tn.ISINKodu = ISINKodu;
            tn.SubeId = SubeId;
            tn.UrunId = UrunId;
            db.TanISINKodlari.Add(tn);
            db.SaveChanges();
            return RedirectToAction("IsinKoduTanimlamalari");

        }
        [Authorize(Roles = "IsinKoduTanimlamalari")]
        public ActionResult ISINGuncelle(int id, string HasatDonemi, string ISINKodu)
        {
            LidosSistemiEntities db = new LidosSistemiEntities();
            int SubeId = Convert.ToInt32(Session["KullaniciSubeId"].ToString());
            var tn = db.TanISINKodlari.Where(t => t.TanISINKodId == id).FirstOrDefault();
            tn.HasatDonemi = HasatDonemi;
            tn.ISINKodu = ISINKodu;
           
           
           
            db.SaveChanges();
            return RedirectToAction("IsinKoduTanimlamalari");

        }
        [Authorize(Roles = "IsinKoduTanimlamalari")]
        [HttpPost]
        public JsonResult GetirUrunler(int? Id)
        {
            //EntityFramework ile veritabanı modelimizi oluşturduk ve

            LidosSistemiEntities db = new LidosSistemiEntities();
           
            //Oluşturduğum sonucları json olarak geriye gönderiyorum
            return Json(db.TanUrunler.Where(t => t.UrunGurupId == Id).Select(x => new
            {
                DepartmentID = x.UrunId,
                DepartmentName = x.UrunAdi,
                DepartmentCode = x.UrunKodu

            }).ToList(), JsonRequestBehavior.AllowGet);

        }


        #endregion
        #region UrunTanımlamaları
        [Authorize(Roles = "UrunTanimlamalari")]
        public ActionResult UrunTanimlamalari()
        {
            LidosSistemiEntities db = new LidosSistemiEntities();
            var UrunUstGruplari = db.TanUrunUstGuruplari.ToList();
            var UrunGuruplari = db.TanUrunGuruplari.ToList();
            var Urunler = db.TanUrunler.ToList();
            return View(Tuple.Create(UrunUstGruplari, UrunGuruplari, Urunler));

        }
        [Authorize(Roles = "UrunTanimlamalari")]
        [HttpPost]
        public ActionResult UrunUstGurubuKaydet(string UstGurupKodu, string UstGurupAdi)
        {
            LidosSistemiEntities db = new LidosSistemiEntities();
            TanUrunUstGuruplari ust = new TanUrunUstGuruplari();
            ust.UrunUstGurupKodu = UstGurupKodu;
            ust.UrunUstGurupAdi = UstGurupAdi;
            db.TanUrunUstGuruplari.Add(ust);
            db.SaveChanges();
            return RedirectToAction("UrunTanimlamalari");

        }
        [Authorize(Roles = "UrunTanimlamalari")]
        [HttpPost]
        public ActionResult UrunUstGurubuGuncelle(int id, string UstGurupKodu, string UstGurupAdi)
        {
            LidosSistemiEntities db = new LidosSistemiEntities();
            var Sorgu = db.TanUrunUstGuruplari.Where(t => t.UrunUstGurupKoduId == id).FirstOrDefault();
            Sorgu.UrunUstGurupAdi = UstGurupAdi;
            Sorgu.UrunUstGurupKodu = UstGurupKodu;
            db.SaveChanges();
            return RedirectToAction("UrunTanimlamalari");

        }
        [Authorize(Roles = "UrunTanimlamalari")]
        [HttpPost]
        public ActionResult UrunGurubuKaydet(string UstGurupKodu, string UstGurupAdi, string UrunUstGurubu, string UstGurupKisaKod)
        {
            LidosSistemiEntities db = new LidosSistemiEntities();
            TanUrunGuruplari ust = new TanUrunGuruplari();
            int UrunUstGrupId = db.TanUrunUstGuruplari.Where(t => t.UrunUstGurupAdi == UrunUstGurubu).Select(t => t.UrunUstGurupKoduId).FirstOrDefault();
            ust.UrunGurupKodu = UstGurupKodu;
            ust.UrunGurupAdi = UstGurupAdi;
            ust.UrunGurupKisaKodu = UstGurupKisaKod;
            ust.UrunUstGurubu = UrunUstGrupId;
            db.TanUrunGuruplari.Add(ust);
            db.SaveChanges();
            return RedirectToAction("UrunTanimlamalari");

        }
        [Authorize(Roles = "UrunTanimlamalari")]
        [HttpPost]
        public ActionResult UrunGurubuGuncelle(int id, string UstGurupKodu, string UstGurupAdi, string UrunUstGurubu, string UstGurupKisaKod)
        {
            LidosSistemiEntities db = new LidosSistemiEntities();
            var ust = db.TanUrunGuruplari.Where(t => t.UrunGurupId == id).FirstOrDefault();
            int UrunUstGrupId = db.TanUrunUstGuruplari.Where(t => t.UrunUstGurupAdi == UrunUstGurubu).Select(t => t.UrunUstGurupKoduId).FirstOrDefault();
            ust.UrunGurupKodu = UstGurupKodu;
            ust.UrunGurupAdi = UstGurupAdi;
            ust.UrunGurupKisaKodu = UstGurupKisaKod;
            ust.UrunUstGurubu = UrunUstGrupId;
            db.SaveChanges();
            return RedirectToAction("UrunTanimlamalari");

        }
        [Authorize(Roles = "UrunTanimlamalari")]
        [HttpPost]
        public ActionResult UrunKaydet(string UrunKodu, string UrunAdi, string UrunDerecesi, string UrunGurubu)
        {
            LidosSistemiEntities db = new LidosSistemiEntities();
            TanUrunler ust = new TanUrunler();
            int UrunGrupId = db.TanUrunGuruplari.Where(t => t.UrunGurupAdi == UrunGurubu).Select(t => t.UrunGurupId).FirstOrDefault();
            ust.UrunKodu = UrunKodu;
            ust.UrunAdi = UrunAdi;
            ust.UrunDerecesi = UrunDerecesi;
            ust.UrunGurupId = UrunGrupId;
            db.TanUrunler.Add(ust);
            db.SaveChanges();
            return RedirectToAction("UrunTanimlamalari");

        }
        [Authorize(Roles = "UrunTanimlamalari")]
        [HttpPost]
        public ActionResult UrunGuncelle(int id, string UrunKodu, string UrunAdi, string UrunDerecesi, string UrunGurubu)
        {
            LidosSistemiEntities db = new LidosSistemiEntities();
            var ust = db.TanUrunler.Where(t => t.UrunId == id).FirstOrDefault();
            int UrunGrupId = db.TanUrunGuruplari.Where(t => t.UrunGurupAdi == UrunGurubu).Select(t => t.UrunGurupId).FirstOrDefault();
            ust.UrunKodu = UrunKodu;
            ust.UrunAdi = UrunAdi;
            ust.UrunDerecesi = UrunDerecesi;
            ust.UrunGurupId = UrunGrupId;

            db.SaveChanges();
            return RedirectToAction("UrunTanimlamalari");

        }
        #endregion

    }
}