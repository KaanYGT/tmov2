﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using YeniOtomasyonSistemi.Models;
using YeniOtomasyonSistemi.Models.Login;

namespace YeniOtomasyonSistemi.Controllers
{
    public class SharedController : Controller
    {
        // GET: Shared
        public ActionResult _Navigation()
        {
            try
            {
                int KullaniciId = Convert.ToInt32(Session["KullaniciId"]);
                
                LidosSistemiEntities db = new LidosSistemiEntities();
                List<UserIdMenuGetir_Result> Deneme = db.UserIdMenuGetir(KullaniciId).ToList();
                
                return PartialView(Deneme);
            }
            catch (Exception)
            {
                return RedirectToAction("Login", "Security");
            }
        }
    }
}