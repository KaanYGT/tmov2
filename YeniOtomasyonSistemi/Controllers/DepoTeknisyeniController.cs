﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using YeniOtomasyonSistemi.Models.Login;
using YeniOtomasyonSistemi.Models;

namespace YeniOtomasyonSistemi.Controllers
{
    [ControlLogin]
    public class DepoTeknisyeniController : Controller
    {
        // GET: DepoTeknisyeni
        [Authorize(Roles = "ArizaKayitistesi")]
        public ActionResult ArizaKayitistesi()
        {
            LidosSistemiEntities db = new LidosSistemiEntities();
            string KullaniciAdi = Session["KullaniciAdi"].ToString();
            int SubeId = Convert.ToInt32(Session["KullaniciSubeId"].ToString());
            string SubeKodu = Session["KullaniciSubeKodu"].ToString();
            if (KullaniciAdi == "Admin")
            {
                var ArizalariGetir = db.DArizaKayitlari.OrderByDescending(t => t.ArizaId).ToList();
                return View(ArizalariGetir);
            }
            else
            {
                var ArizalariGetir = db.DArizaKayitlari.Where(t => t.Subesi == SubeKodu).OrderByDescending(t => t.ArizaId).ToList();
                return View(ArizalariGetir);
            }

        }
        [Authorize(Roles = "ArizaKayitistesi")]
        public ActionResult ArizaSonucuEkle(int? id)
        {
            LidosSistemiEntities db = new LidosSistemiEntities();
            var ArizalariGetir = db.DArizaKayitlari.Where(t => t.ArizaId == id).FirstOrDefault();
            return View(ArizalariGetir);

        }
        [Authorize(Roles = "ArizaKayitistesi")]
        [HttpPost]
        public ActionResult ArizaSonucuEkle(int? id, string SonucZamani, string ArizaSonucu)
        {

            LidosSistemiEntities db = new LidosSistemiEntities();
            var ArizalariGetir = db.DArizaKayitlari.Where(t => t.ArizaId == id).FirstOrDefault();
            ArizalariGetir.SonucTarihi = SonucZamani;
            ArizalariGetir.Sonuc = ArizaSonucu;
            ArizalariGetir.Durumu = "Sonuçlandı";
            db.SaveChanges();
            return RedirectToAction("ArizaKayitistesi", "DepoTeknisyeni");

        }
        [Authorize(Roles = "ArizaKayitistesi")]
        [HttpPost]
        public ActionResult ArizaKaydet(string ArizaBasligi, string ArizaTarihi, string ArızaAciklamasi)
        {
            string KullaniciAdi = Session["KullaniciAdi"].ToString();
            string SubeKodu = Session["KullaniciSubeKodu"].ToString();
            LidosSistemiEntities db = new LidosSistemiEntities();
            DArizaKayitlari dp = new DArizaKayitlari();
            dp.ArizaIcerigi = ArızaAciklamasi;
            dp.ArizaKonusu = ArizaBasligi;
            dp.Durumu = "Arıza Kaydı Açıldı";
            dp.Sonuc = " ";
            dp.SonucTarihi = " ";

            dp.ArizaZamani = ArizaTarihi;
            dp.KaydedenUser = KullaniciAdi;
            dp.KayitTarihi = DateTime.Now;
            dp.Subesi = SubeKodu;
            db.DArizaKayitlari.Add(dp);
            db.SaveChanges();
            return RedirectToAction("ArizaKayitistesi", "DepoTeknisyeni");

        }
        [Authorize(Roles = "ArizaKayitistesi")]
        public ActionResult ArizaYazdir(int? id)
        {
            string KullaniciAdi = Session["KullaniciAdi"].ToString();
            string SubeKodu = Session["KullaniciSubeKodu"].ToString();
            LidosSistemiEntities db = new LidosSistemiEntities();
            if (KullaniciAdi == "Admin")
            {
                var BakimlariGetir = db.DArizaKayitlari.Where(t => t.ArizaId == id).FirstOrDefault();
                return View(BakimlariGetir);
            }
            else
            {
                var BakimlariGetir = db.DArizaKayitlari.Where(t => t.Subesi == SubeKodu && t.ArizaId == id).FirstOrDefault();
                return View(BakimlariGetir);
            }

        }
        [Authorize(Roles = "DepoBakimListesi")]
        public ActionResult DepoBakimListesi()
        {
            LidosSistemiEntities db = new LidosSistemiEntities();
            string KullaniciAdi = Session["KullaniciAdi"].ToString();
            string SubeKodu = Session["KullaniciSubeKodu"].ToString();
            if (KullaniciAdi == "Admin")
            {
                var BakimlariGetir = db.DepoBakimlari.OrderByDescending(t => t.BakimId).ToList();
                return View(BakimlariGetir);
            }
            else
            {
                var BakimlariGetir = db.DepoBakimlari.Where(t => t.SubeAdi == SubeKodu).OrderByDescending(t => t.BakimId).ToList();
                return View(BakimlariGetir);
            }

        }
        [Authorize(Roles = "DepoBakimListesi")]
        [HttpPost]
        public ActionResult BakimKaydet(string BakimBasligi, string BakimTarihi, string BakimAciklamasi, string BakimGurubu)
        {
            string KullaniciAdi = Session["KullaniciAdi"].ToString();
            string SubeKodu = Session["KullaniciSubeKodu"].ToString();
            LidosSistemiEntities db = new LidosSistemiEntities();
            DepoBakimlari dp = new DepoBakimlari();
            dp.BakimAciklamasi = BakimAciklamasi;
            dp.BakimAdi = BakimBasligi;
            dp.BakimGurubu = BakimGurubu;
            dp.BakimTarihi = BakimTarihi;
            dp.KaydedenKullanici = KullaniciAdi;
            dp.KayitZamani = DateTime.Now;
            dp.SubeAdi = SubeKodu;
            db.DepoBakimlari.Add(dp);
            db.SaveChanges();
            return RedirectToAction("DepoBakimListesi", "DepoTeknisyeni");
        }
        public ActionResult BakimYazdir(int? id)
        {
            LidosSistemiEntities db = new LidosSistemiEntities();
            string KullaniciAdi = Session["KullaniciAdi"].ToString();
            string SubeKodu = Session["KullaniciSubeKodu"].ToString();
            if (KullaniciAdi == "Admin")
            {
                var BakimlariGetir = db.DepoBakimlari.Where(t => t.BakimId == id).FirstOrDefault();
                return View(BakimlariGetir);
            }
            else
            {
                var BakimlariGetir = db.DepoBakimlari.Where(t => t.SubeAdi == SubeKodu && t.BakimId == id).FirstOrDefault();
                return View(BakimlariGetir);
            }

        }
    }
}