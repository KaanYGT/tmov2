﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using YeniOtomasyonSistemi.Models;
using YeniOtomasyonSistemi.Models.Login;

namespace YeniOtomasyonSistemi.Controllers
{
    [ControlLogin]
    public class EksperController : Controller
    {
        [Authorize(Roles = "KayisiDepoOnKayitListesi")]
        public ActionResult KayisiDepoOnKayitListesi()
        {
            LidosSistemiEntities db = new LidosSistemiEntities();
            string KullaniciAdi = Session["KullaniciAdi"].ToString();
            int SubeId = Convert.ToInt32(Session["KullaniciSubeId"].ToString());
            string SubeKodu = Session["KullaniciSubeKodu"].ToString();


            var ListeSorgula = db.EksperKayisiDepoOnKayit.Where(t => t.SubeKodu == SubeKodu).ToList();
            var GorevliBul = db.TanGorevliler.Where(t => t.GorevliSubeId == SubeId && t.GorevBirimi == "Eksper").ToList();
            var RaflariGetir = db.EksperKayisiRaflar.ToList();
            var UrunListesi = db.TanUrunler.ToList();
            return View(Tuple.Create( ListeSorgula,GorevliBul,RaflariGetir,UrunListesi));
        }

        [Authorize(Roles = "KayisiDepoListesi")]
        public ActionResult KayisiDepoListesi()
        {
            LidosSistemiEntities db = new LidosSistemiEntities();
            string KullaniciAdi = Session["KullaniciAdi"].ToString();
            int SubeId = Convert.ToInt32(Session["KullaniciSubeId"].ToString());
            string SubeKodu = Session["KullaniciSubeKodu"].ToString();


            var ListeSorgula = db.EksperKayisiDepolar.Where(t => t.SubeKodu == SubeKodu).ToList();
            
            return View(ListeSorgula);
        }

        [Authorize(Roles = "KayisiDepoListesi")]
        [HttpGet]
        public ActionResult KayisiRafListesi(int? id)
        {
            LidosSistemiEntities db = new LidosSistemiEntities();
            string KullaniciAdi = Session["KullaniciAdi"].ToString();
            int SubeId = Convert.ToInt32(Session["KullaniciSubeId"].ToString());
            string SubeKodu = Session["KullaniciSubeKodu"].ToString();


            var ListeSorgula = db.EksperKayisiRaflar.Where(t => t.RafinBulunduguDepoId == id).ToList();
            var UrunleriGetir = db.TanUrunler.Where(t => t.TanUrunGuruplari.TanUrunUstGuruplari.UrunUstGurupAdi == "Kuru Kayısı").ToList();
            var DepoGetir = db.EksperKayisiDepolar.Where(t => t.DepoId == id).FirstOrDefault();
            return View(Tuple.Create(ListeSorgula,UrunleriGetir,DepoGetir));
        }
        [Authorize(Roles = "KayisiDepoListesi")]
        [HttpPost]
        public ActionResult RaftakiUrunuGuncelle(int? id,int UrunId,int depoId)
        {
            LidosSistemiEntities db = new LidosSistemiEntities();
            string KullaniciAdi = Session["KullaniciAdi"].ToString();
            int SubeId = Convert.ToInt32(Session["KullaniciSubeId"].ToString());
            string SubeKodu = Session["KullaniciSubeKodu"].ToString();

            var Rafbul = db.EksperKayisiRaflar.Where(t => t.RafId == id).FirstOrDefault();
            Rafbul.RaftaBulunanUrunId = UrunId;
            db.SaveChanges();
            var RafaAitKasalar = db.EksperKayisiKasalar.Where(t => t.RafId == id).ToList();
            for (int i = 0; i < RafaAitKasalar.Count; i++)
            {
                
                RafaAitKasalar[i].IcindekiUrunId = UrunId;
                RafaAitKasalar[i].UrunVarmi = "Hayır";
                db.SaveChanges();
            }

            return RedirectToAction("KayisiRafListesi","Eksper", depoId);
        }
        [Authorize(Roles = "KayisiDepoListesi")]
        [HttpPost]
        public ActionResult KayisiYeniRafEkle(int id,string RafAdi,int RafKatSayisi,int RafBolumSayisi,int RafBolumdekiKasaSayisi,int KasaAgirligi,int UrunId)
        {
            LidosSistemiEntities db = new LidosSistemiEntities();
            string KullaniciAdi = Session["KullaniciAdi"].ToString();
            int SubeId = Convert.ToInt32(Session["KullaniciSubeId"].ToString());
            string SubeKodu = Session["KullaniciSubeKodu"].ToString();
            string DepoAdi = db.EksperKayisiDepolar.Where(t => t.DepoId == id).Select(t => t.DepoAdi).FirstOrDefault().Substring(0, 1);
            string YeniRafAdi = DepoAdi + RafAdi;
            int RafToplamKapasitesi = RafKatSayisi * RafBolumSayisi * RafBolumdekiKasaSayisi * KasaAgirligi;
            int ToplamKasaMiktari = RafKatSayisi * RafBolumSayisi * RafBolumdekiKasaSayisi;
            EksperKayisiRaflar kr = new EksperKayisiRaflar();
            kr.HerBolumdekiKasaSayisi = RafBolumdekiKasaSayisi;
            kr.RafAdi = YeniRafAdi;
            kr.RafBolumSayisi = RafBolumSayisi;
            kr.RafDoluluk = 0;
            kr.RafBosAlan= RafToplamKapasitesi;
            kr.RafinBulunduguDepoId = id;
            kr.RafKapasitesi = RafToplamKapasitesi;
            kr.RafKatSayisi = RafKatSayisi;
            kr.RaftaBulunanUrunId = UrunId;
            kr.SubeKodu = SubeKodu;
            db.EksperKayisiRaflar.Add(kr);
            db.SaveChanges();
           
            int RafIdBul = db.EksperKayisiRaflar.Where(t => t.SubeKodu == SubeKodu).OrderByDescending(t=>t.RafId).Select(t => t.RafId).FirstOrDefault();

            for (int i = 1; i <= ToplamKasaMiktari; i++)
            {
                EksperKayisiKasalar ks = new EksperKayisiKasalar();
                ks.IcindekiUrunId = UrunId;
                ks.KasaAdi = YeniRafAdi + "K-" + i.ToString();
                ks.KasaBosMiktar = KasaAgirligi;
                ks.KasaDoluMiktar = 0;
                ks.KasaToplamKapasitesi = KasaAgirligi;
                ks.RafId = RafIdBul;
                ks.SonUrunYuklemeZamani = DateTime.Now;
                ks.UrunVarmi = "Hayır";
                db.EksperKayisiKasalar.Add(ks);
                db.SaveChanges();
            }

          
            return RedirectToAction("KayisiRafListesi", "Eksper", new { id});
        }

        [Authorize(Roles = "KayisiDepoListesi")]
        [HttpGet]
        public ActionResult KayisiKasaListesi(int? id)
        {
            LidosSistemiEntities db = new LidosSistemiEntities();
            int SubeId = Convert.ToInt32(Session["KullaniciSubeId"].ToString());
            string SubeKodu = Session["KullaniciSubeKodu"].ToString();
            var ListeSorgula = db.EksperKayisiKasalar.Where(t => t.RafId == id).OrderBy(w=>w.KasaId).ToList();
            var RafiGetir = db.EksperKayisiRaflar.Where(t => t.RafId == id).FirstOrDefault();
            var DepoGetir = db.EksperKayisiDepolar.Where(t => t.DepoId == id).FirstOrDefault();
            return View(Tuple.Create(ListeSorgula, RafiGetir, DepoGetir));
        }
        
    }
}