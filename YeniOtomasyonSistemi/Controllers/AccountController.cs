﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using YeniOtomasyonSistemi.Models;
using YeniOtomasyonSistemi.Models.Login;

namespace YeniOtomasyonSistemi.Controllers
{
    [ControlLogin]
    public class AccountController : Controller
    {
        // GET: Account
        public ActionResult Profil()
        {
            int iKullanici = Convert.ToInt32(Session["KullaniciId"]);
            LidosSistemiEntities db = new LidosSistemiEntities();
            var vKullanici = db.LKullanicilar.Where(w => w.KullaniciId == iKullanici).FirstOrDefault();
            return View(Tuple.Create(vKullanici));
        }

        public ActionResult ProfilGuncelle(LKullanicilar gelenKullanici)
        {
            try
            {
                LidosSistemiEntities db = new LidosSistemiEntities();
                int iKullanici = Convert.ToInt32(Session["KullaniciId"]);
                var vHangiKullanici = db.LKullanicilar.Where(w => w.KullaniciId == iKullanici).FirstOrDefault();
                vHangiKullanici.KullaniciSifresi = gelenKullanici.KullaniciSifresi;
                db.SaveChanges();
                TempData["approval"] = "Güncelleme Tamamlandı.";
                return RedirectToAction("Profil");
            }
            catch (Exception)
            {
                TempData["rejection"] = "Güncelleme işlemi sırasında bir hata oluştu.";
                return RedirectToAction("Profil");
            }
        }
    }
}