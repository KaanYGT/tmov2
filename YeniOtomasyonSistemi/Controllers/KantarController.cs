﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using YeniOtomasyonSistemi.Models.Login;
using YeniOtomasyonSistemi.Models;

namespace YeniOtomasyonSistemi.Controllers
{
    [ControlLogin]
    public class KantarController : Controller
    {
        // GET: Kantar
        [Authorize(Roles = "MudiKaydet")]
        public ActionResult MudiKaydet()
        {
            LidosSistemiEntities db = new LidosSistemiEntities();
            var sorgu2 = db.KantarMudiAraciKurum.ToList();
            return View(sorgu2);

        }
        [Authorize(Roles = "MudiKaydet")]
        [HttpPost]
        public ActionResult MudiKaydet(string MudiTckVKN, string MudiAdSoyad, string Telefon, string MudiIl, string MudiIlce, string MudiAcikAdres, string MudiAraciKurum, string BankaHesapNo)
        {
            LidosSistemiEntities db = new LidosSistemiEntities();
            KantarMudiBilgileri md = new KantarMudiBilgileri();
            string SubeKodu = Session["KullaniciSubeKodu"].ToString();
            string KullaniciAdi = Session["KullaniciAdi"].ToString();
            int araciKurumId = db.KantarMudiAraciKurum.Where(t => t.AraciKurumAdi == MudiAraciKurum).Select(t => t.AraciKurumId).FirstOrDefault();
            md.AdresDevami = MudiAcikAdres;
            md.AdresIl = MudiIl;
            md.AdresIlce = MudiIlce;
            md.AraciKurumId = araciKurumId;
            md.BankaHesapNo = BankaHesapNo;
            md.KaydedenSubeKodu = SubeKodu;
            md.KaydedenUser = KullaniciAdi;
            md.KayitTarihi = DateTime.Now;
            md.MudiAdiSoyadi = MudiAdSoyad;
            md.MudiTcVkn = MudiTckVKN;
            md.Telefon = Telefon;
            db.KantarMudiBilgileri.Add(md);
            db.SaveChanges();

            return RedirectToAction("MudiListesi");

        }
        [Authorize(Roles = "MudiListesi")]
        public ActionResult MudiListesi()
        {
            LidosSistemiEntities db = new LidosSistemiEntities();
            string SubeKodu = Session["KullaniciSubeKodu"].ToString();
            string KullaniciAdi = Session["KullaniciAdi"].ToString();
            List<KantarMudiBilgileri> sorgu ;
            if (KullaniciAdi=="Admin")
            {
                 sorgu = db.KantarMudiBilgileri.ToList();
            }
            else
            {
                sorgu = db.KantarMudiBilgileri.Where(t => t.KaydedenSubeKodu == SubeKodu).ToList();
            }
            var sorgu2 = db.KantarMudiAraciKurum.ToList();
            return View(Tuple.Create( sorgu,sorgu2));

        }
        [Authorize(Roles = "MudiKaydet")]
        public ActionResult MudiDuzenle(int ?id)
        {
            LidosSistemiEntities db = new LidosSistemiEntities();
            string SubeKodu = Session["KullaniciSubeKodu"].ToString();
            string KullaniciAdi = Session["KullaniciAdi"].ToString();
            var sorgu = db.KantarMudiBilgileri.Where(t=>t.MudiId==id&&t.KaydedenSubeKodu==SubeKodu).FirstOrDefault();
            var sorgu2 = db.KantarMudiAraciKurum.ToList();
            return View(Tuple.Create(sorgu,sorgu2));

        }
        [Authorize(Roles = "MudiKaydet")]
        [HttpPost]
        public ActionResult MudiDuzenle(string MudiTckVKN, string MudiAdSoyad, string Telefon, string MudiIl, string MudiIlce, string MudiAcikAdres, string MudiAraciKurum, string BankaHesapNo,int id)
        {
            LidosSistemiEntities db = new LidosSistemiEntities();
            KantarMudiBilgileri md = db.KantarMudiBilgileri.Where(t=>t.MudiId==id).FirstOrDefault();
            string SubeKodu = Session["KullaniciSubeKodu"].ToString();
            string KullaniciAdi = Session["KullaniciAdi"].ToString();
            int araciKurumId = db.KantarMudiAraciKurum.Where(t => t.AraciKurumAdi == MudiAraciKurum).Select(t => t.AraciKurumId).FirstOrDefault();
            md.AdresDevami = MudiAcikAdres;
            md.AdresIl = MudiIl;
            md.AdresIlce = MudiIlce;
            md.AraciKurumId = araciKurumId;
            md.BankaHesapNo = BankaHesapNo;
            md.KaydedenSubeKodu = SubeKodu;
            md.KaydedenUser = KullaniciAdi;
            md.KayitTarihi = DateTime.Now;
            md.MudiAdiSoyadi = MudiAdSoyad;
            md.MudiTcVkn = MudiTckVKN;
            md.Telefon = Telefon;
            
            db.SaveChanges();

            return RedirectToAction("MudiListesi");

        }
        [Authorize(Roles = "YeniTartimKaydi")]
        public ActionResult YeniTartimKaydi()
        {
            LidosSistemiEntities db = new LidosSistemiEntities();
           
            string SubeKodu = Session["KullaniciSubeKodu"].ToString();
            int SubeId = Convert.ToInt32(Session["KullaniciSubeId"].ToString());
            var GorevliListesi = db.TanGorevliler.Where(t => t.GorevliSubeId == SubeId && t.GorevBirimi == "Kantar").ToList();
            var UrunGuruplari = db.TanUrunUstGuruplari.ToList();
          
            return View(Tuple.Create(UrunGuruplari,  GorevliListesi));
            

        }

        [Authorize(Roles = "YeniTartimKaydi")]
        [HttpPost]
        public ActionResult YeniTartimKaydi(string MudiTckVKN,string AracPlakasi,string AracTuru,string Urun,string ElusTalep,string TartimiYapanGorevli)
        {
            LidosSistemiEntities db = new LidosSistemiEntities();
            int GorevliId = db.TanGorevliler.Where(t => t.GorevliAdiSoyadi == TartimiYapanGorevli).Select(t => t.GorevliId).FirstOrDefault();
            string SubeKodu = Session["KullaniciSubeKodu"].ToString();
            string KullaniciAdi = Session["KullaniciAdi"].ToString();
            var KullaniciGetir = db.KantarMudiBilgileri.Where(t => t.MudiTcVkn == MudiTckVKN&&t.KaydedenSubeKodu==SubeKodu).FirstOrDefault();
            if (KullaniciGetir!=null)
            {
                var KantarKayitNumarasi = db.KantarTartimKayitlari.Where(t => t.KaydedenSube == SubeKodu).OrderByDescending(t => t.KantarKayitId).Select(t => t.KantarKayitNo).FirstOrDefault();
                KantarTartimKayitlari kt = new KantarTartimKayitlari();
                kt.AracPlakasi = AracPlakasi;
                kt.AractakiUrun = Urun;
                kt.AracTuru = AracTuru;
                kt.BirinciTartim = 0;
                kt.DegisimZamani = DateTime.Now;
                kt.DegistirenUser = KullaniciAdi;
                kt.IkinciTartim = 0;
                if (KantarKayitNumarasi!=null)
                {
                    kt.KantarKayitNo = KantarKayitNumarasi + 1;
                }
                else
                {
                    kt.KantarKayitNo = 1;
                }
                kt.KaydedenSube = SubeKodu;
                kt.KaydiYapanUser = KullaniciAdi;
                kt.TartimGorevliId = GorevliId;
                kt.KayitZamani =DateTime.Now;
                kt.MudiElusTalebi = ElusTalep;
                kt.NetHesaplanan = 0;
                kt.MudiId = KullaniciGetir.MudiId;
                db.KantarTartimKayitlari.Add(kt);
                db.SaveChanges();
                return RedirectToAction("TartimKaydiListesi");
            }
            else
            {
                ViewBag.mesaj = "Girilen Tc Numarasına ait kayıt bulunmadığından lütfen önce mudi hesabı oluşturun";
                return RedirectToAction("MudiKaydet");
            }
          

        }

        [Authorize(Roles = "TartimKaydiListesi")]
        [HttpGet]
        public ActionResult TartimKaydiListesi()
        {
            LidosSistemiEntities db = new LidosSistemiEntities();
            DateTime Yarin = DateTime.Now.AddDays(1);
            DateTime Dun = DateTime.Now.AddDays(-1);
            string SubeKodu = Session["KullaniciSubeKodu"].ToString();
            int SubeId = Convert.ToInt32(Session["KullaniciSubeId"].ToString());
            var GorevliListesi = db.TanGorevliler.Where(t => t.GorevliSubeId == SubeId && t.GorevBirimi == "Kantar").ToList();
            var UrunGuruplari = db.TanUrunUstGuruplari.ToList();
            var TartimListesi = db.KantarTartimKayitlari.Where(t => t.KaydedenSube == SubeKodu && t.KayitZamani == DateTime.Today).ToList();
            return View(Tuple.Create(UrunGuruplari, TartimListesi, GorevliListesi));
        }

        [Authorize(Roles = "TartimKaydiListesi")]
        [HttpPost]
        public ActionResult TartimKaydiListesi(DateTime tarih)
        {
            LidosSistemiEntities db = new LidosSistemiEntities();
            string SubeKodu = Session["KullaniciSubeKodu"].ToString();
            int SubeId = Convert.ToInt32(Session["KullaniciSubeId"].ToString());


            var GorevliListesi = db.TanGorevliler.Where(t => t.GorevliSubeId == SubeId && t.GorevBirimi == "Kantar").ToList();
            var UrunGuruplari = db.TanUrunUstGuruplari.ToList();

            var TartimListesi = db.KantarTartimKayitlari.Where(t => t.KaydedenSube == SubeKodu && t.KayitZamani >= tarih).ToList();
            return View(Tuple.Create(UrunGuruplari, TartimListesi, GorevliListesi));
        }

        [Authorize(Roles = "TartimKaydiListesi")]
        [HttpPost]
        public JsonResult MudiyiGetir()
        {
            LidosSistemiEntities db = new LidosSistemiEntities();
            var MudiListesi = db.KantarMudiBilgileri.ToList();

            string MudiListesiTopla = "";
            foreach (var item in MudiListesi)
            {
                MudiListesiTopla = MudiListesiTopla + "," + item.MudiTcVkn;
            }
            string[] ToplamMudiListesi = new string[]
            {
                ""
            };
            ToplamMudiListesi = MudiListesiTopla.Split(',');
            return Json(ToplamMudiListesi, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "TartimKaydiListesi")]
        [HttpPost]
        public ActionResult IlkTartimKaydet(int ?Id,int UrunMiktari)
        {
            LidosSistemiEntities db = new LidosSistemiEntities();
            var KayitBul = db.KantarTartimKayitlari.Where(t => t.KantarKayitId == Id).FirstOrDefault();
            KayitBul.BirinciTartim = UrunMiktari;
            string KullaniciAdi = Session["KullaniciAdi"].ToString();
            string SubeKodu = Session["KullaniciSubeKodu"].ToString(); 
            KayitBul.DegistirenUser = KullaniciAdi;
            KayitBul.DegisimZamani = DateTime.Now;
            db.SaveChanges();

            var SonNumuneNumarasi = db.LabNumuneAlma.Where(t => t.KaydedenSubesi == SubeKodu).OrderByDescending(t => t.NumuneId).Select(t => t.NumuneNo).FirstOrDefault();
            LabNumuneAlma na = new LabNumuneAlma();
            na.AmbalajAdeti = "3";
            na.AmbalajSekli = "Dökme";
            na.DegisiklikZamani = DateTime.Now;
            na.DegistirenPersonel = KullaniciAdi;
            na.KantarKayitId = Id;
            na.KaydedenPersonel = KullaniciAdi;
            na.KaydedenSubesi = SubeKodu;
            na.KayitDurumu = "Beklemede";
            na.KayitZamani = DateTime.Now;
            na.NumuneAgirligi = "0";
            na.NumuneAlindigiYer = "Araç Üstü";
            na.NumuneMuhurNo = "0";
            if (SonNumuneNumarasi!=null)
            {
                int NumuneNo = (int)SonNumuneNumarasi;
                na.NumuneNo = NumuneNo + 1;
            }
            else
            {
                na.NumuneNo =  1;
            }
            na.NumuneyiAlanGorevliId = 1;
            db.LabNumuneAlma.Add(na);
            db.SaveChanges();
            return RedirectToAction("TartimKaydiListesi");

        }
        [Authorize(Roles = "TartimKaydiListesi")]

        public ActionResult IlkTartimYazdir(int? Id)
        {
            LidosSistemiEntities db = new LidosSistemiEntities();
            var KayitBul = db.KantarTartimKayitlari.Where(t => t.KantarKayitId == Id).FirstOrDefault();
            return View(KayitBul);
            
        }
        [Authorize(Roles = "TartimKaydiListesi")]
        [HttpPost]
        public ActionResult IkinciTartimKaydet(int? Id, int UrunMiktari)
        {
            LidosSistemiEntities db = new LidosSistemiEntities();
            var KayitBul = db.KantarTartimKayitlari.Where(t => t.KantarKayitId == Id).FirstOrDefault();
            int ilkTartimMiktari = (int)KayitBul.BirinciTartim;
            int NetMiktar = ilkTartimMiktari - UrunMiktari;
            KayitBul.IkinciTartim = UrunMiktari;
            KayitBul.NetHesaplanan = NetMiktar;
            string KullaniciAdi = Session["KullaniciAdi"].ToString();
            KayitBul.DegistirenUser = KullaniciAdi;
            KayitBul.DegisimZamani = DateTime.Now;
            db.SaveChanges();
            return RedirectToAction("TartimKaydiListesi");

        }
    }
}