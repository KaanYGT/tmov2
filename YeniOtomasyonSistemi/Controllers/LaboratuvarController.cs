﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using YeniOtomasyonSistemi.Models.Login;
using YeniOtomasyonSistemi.Models;
using static YeniOtomasyonSistemi.Models.KayisiLaboratuvarMetodlari;

namespace YeniOtomasyonSistemi.Controllers
{
    [ControlLogin]
    public class LaboratuvarController : Controller
    {
        // GET: Laboratuvar
        [Authorize(Roles = "NumuneAlmaListesi")]
        [HttpGet]
        public ActionResult NumuneAlmaListesi()
        {
            LidosSistemiEntities db = new LidosSistemiEntities();
            string KullaniciAdi = Session["KullaniciAdi"].ToString();
            string SubeKodu = Session["KullaniciSubeKodu"].ToString();
            int SubeId = Convert.ToInt32(Session["KullaniciSubeId"].ToString());
            var NumuneListesi = db.LabNumuneAlma.Where(t => t.KaydedenSubesi == SubeKodu && t.KayitZamani == DateTime.Today).ToList();
            var Gorevliler = db.TanGorevliler.Where(t => t.GorevliSubeId == SubeId&&t.GorevBirimi== "Numune Alma").ToList(); // Numune alan görevli ile kaydeden görevli bazı durum
            return View(Tuple.Create(NumuneListesi, Gorevliler));

        }

        [Authorize(Roles = "NumuneAlmaListesi")]
        [HttpPost]
        public ActionResult NumuneAlmaListesi(DateTime tarih)
        {
            LidosSistemiEntities db = new LidosSistemiEntities();
            string KullaniciAdi = Session["KullaniciAdi"].ToString();
            string SubeKodu = Session["KullaniciSubeKodu"].ToString();
            int SubeId = Convert.ToInt32(Session["KullaniciSubeId"].ToString());
            var NumuneListesi = db.LabNumuneAlma.Where(t => t.KaydedenSubesi == SubeKodu && t.KayitZamani >= tarih).ToList();
            var Gorevliler = db.TanGorevliler.Where(t => t.GorevliSubeId == SubeId && t.GorevBirimi == "Numune Alma").ToList(); // Numune alan görevli ile kaydeden görevli bazı durum
            return View(Tuple.Create(NumuneListesi, Gorevliler));

        }

        [Authorize(Roles = "NumuneAlmaListesi")]
        [HttpPost]
        public ActionResult NumuneAlmaKaydet(int id,string MuhurNo,string NumuneAlinmaYeri,string AmbalajSekli,string NumuneAgirlik,string NumuneAdet,string Gorevli)
        {
            LidosSistemiEntities db = new LidosSistemiEntities();
            string KullaniciAdi = Session["KullaniciAdi"].ToString();
            string SubeKodu = Session["KullaniciSubeKodu"].ToString();
            int GorevliId = db.TanGorevliler.Where(t => t.GorevliAdiSoyadi == Gorevli).Select(t => t.GorevliId).FirstOrDefault();
            var na = db.LabNumuneAlma.Where(t => t.NumuneId == id).FirstOrDefault();
            na.AmbalajAdeti = NumuneAdet;
            na.AmbalajSekli = AmbalajSekli;
            na.DegisiklikZamani = DateTime.Now;
            na.DegistirenPersonel = KullaniciAdi;
            na.KaydedenPersonel = KullaniciAdi;
            na.KaydedenSubesi = SubeKodu;
            na.KayitDurumu = "Tamamlandı";      
            na.NumuneAgirligi = NumuneAgirlik;
            na.NumuneAlindigiYer = NumuneAlinmaYeri;
            na.NumuneMuhurNo = MuhurNo;
            na.NumuneyiAlanGorevliId = GorevliId;
            db.SaveChanges();

            var NumuneBilgileri = db.LabNumuneAlma.Where(t => t.NumuneId == id).FirstOrDefault();
            LabLaboratuvarAnalizListesi lb = new LabLaboratuvarAnalizListesi();
            var SonAnalizNumarasi = db.LabLaboratuvarAnalizListesi.Where(t => t.SubeKodu == SubeKodu).OrderByDescending(t => t.LabAnalizId).Select(t => t.AnalizNo).FirstOrDefault();
            string UrunAdi = NumuneBilgileri.KantarTartimKayitlari.AractakiUrun;
            int UrunGurubuId = db.TanUrunUstGuruplari.Where(t => t.UrunUstGurupAdi == UrunAdi).Select(t=>t.UrunUstGurupKoduId).FirstOrDefault();
            if (SonAnalizNumarasi != null)
            {
                int AnalizN = Convert.ToInt32(SonAnalizNumarasi);
                lb.AnalizNo = (AnalizN + 1).ToString();
            }
            else
            {
                lb.AnalizNo = "1";
            }
            lb.AnalizTarihi = DateTime.Now;
            lb.Durumu = "Beklemede";
            lb.KantarKayitNo = NumuneBilgileri.KantarTartimKayitlari.KantarKayitNo.ToString();
            lb.KaydedenUser = KullaniciAdi;
            lb.KayitTarihi = DateTime.Now;
            lb.NumuneNo = NumuneBilgileri.NumuneNo.ToString();
            lb.SubeKodu = SubeKodu;
            lb.UrunUstGurubuId = UrunGurubuId;
            db.LabLaboratuvarAnalizListesi.Add(lb);
            db.SaveChanges();
            return RedirectToAction("NumuneAlmaListesi");

        }

        [Authorize(Roles = "LaboratuvarAnalizListesi")]
        [HttpGet]
        public ActionResult LaboratuvarAnalizListesi()
        {
            LidosSistemiEntities db = new LidosSistemiEntities();
            string KullaniciAdi = Session["KullaniciAdi"].ToString();
            string SubeKodu = Session["KullaniciSubeKodu"].ToString();
            int SubeId = Convert.ToInt32(Session["KullaniciSubeId"].ToString());
            DateTime Yarin = DateTime.Now.AddDays(1);
            DateTime Dun = DateTime.Now.AddDays(-1);

            var AnalizListesi = db.LabLaboratuvarAnalizListesi.Where(t => t.SubeKodu == SubeKodu && t.KayitTarihi >= Dun && t.KayitTarihi <= Yarin).ToList();
            var Gorevliler = db.TanGorevliler.Where(t => t.GorevliSubeId == SubeId && t.GorevBirimi == "Numune Alma").ToList(); // Numune alan görevli ile kaydeden görevli bazı durum
            return View(Tuple.Create(AnalizListesi, Gorevliler));

        }

        [Authorize(Roles = "LaboratuvarAnalizListesi")]
        [HttpPost]
        public ActionResult LaboratuvarAnalizListesi(DateTime tarih)
        {
            LidosSistemiEntities db = new LidosSistemiEntities();
            string KullaniciAdi = Session["KullaniciAdi"].ToString();
            string SubeKodu = Session["KullaniciSubeKodu"].ToString();
            int SubeId = Convert.ToInt32(Session["KullaniciSubeId"].ToString());
            var vAnalizListesi = db.LabLaboratuvarAnalizListesi.Where(t => t.SubeKodu == SubeKodu && t.KayitTarihi >= tarih).ToList();
            var Gorevliler = db.TanGorevliler.Where(t => t.GorevliSubeId == SubeId && t.GorevBirimi == "Numune Alma").ToList(); // Numune alan görevli ile kaydeden görevli bazı durum
            return View(Tuple.Create(vAnalizListesi, Gorevliler));

        }

        [Authorize(Roles = "LaboratuvarAnalizListesi")]
        [HttpGet]
        public ActionResult LaboratuvarKayisiAnalizi(int id)
        {
            LidosSistemiEntities db = new LidosSistemiEntities();
            string KullaniciAdi = Session["KullaniciAdi"].ToString();
            string SubeKodu = Session["KullaniciSubeKodu"].ToString();
            int SubeId = Convert.ToInt32(Session["KullaniciSubeId"].ToString());
            var Sorgu = db.LabKayisiAnalizDetaylari.Where(t => t.AnalizListeId == id&&t.SubeKodu==SubeKodu).FirstOrDefault();
            var AnalizGenelBilgileri = db.LabLaboratuvarAnalizListesi.Where(t => t.SubeKodu == SubeKodu && t.LabAnalizId == id).FirstOrDefault();
            if (Sorgu!=null)
            {
                AnalizKalemleri ak = new AnalizKalemleri();
                ak.AgirKirliTane =Convert.ToDecimal( Sorgu.AgirKirliTane);
                ak.BocekHasarli = Convert.ToDecimal(Sorgu.BocekHasarli);
                ak.CekirdekliKayisi= Convert.ToDecimal(Sorgu.CekirdekliKayisi);
                ak.CilliTane= Convert.ToDecimal(Sorgu.CilliTane); 
                ak.DoluYaraliTane= Convert.ToDecimal(Sorgu.DoluYaraliTane);
                ak.GunesYanigi= Convert.ToDecimal(Sorgu.GunesYanigi);
                ak.HafifKirliTane= Convert.ToDecimal(Sorgu.HafifKirliTane);
                ak.HasarliTane= Convert.ToDecimal(Sorgu.HasarliTane);
                ak.KukurtOrani = Convert.ToInt32(Sorgu.KukurtOrani);
                ak.OluBocekParcalari= Convert.ToDecimal(Sorgu.OluBocekParcalari);
                ak.RenkUyumsuzlugu = Convert.ToDecimal(Sorgu.RenkUyumsuzlugu);
                ak.RutubetOrani= Convert.ToDecimal(Sorgu.RutubetOrani);
                ak.TaneAdeti= Convert.ToInt32(Sorgu.TaneAdeti);
                ak.YabanciMaddeBitkisel= Convert.ToDecimal(Sorgu.YabanciMaddeBitkisel);
                ak.YabanciMaddeTehlikeli= Convert.ToDecimal(Sorgu.YabanciMaddeTehlikeli);
                ak.AnalizListesiId = id;
                
                AnalizSonuclari Ans = new AnalizSonuclari();
                Ans.BoyDerecesi = Convert.ToInt32(Sorgu.SonucBoyDerecesi);
                Ans.GurupAdi = Sorgu.SonucGurupAdi;
                Ans.KayisiKukurtTipi = Sorgu.SonucKayisiKukurtTipi;
                Ans.RedEdilmeNedeni = Sorgu.SonucRedEdilmeAciklamasi;
                Ans.ToplamKusur = Convert.ToDecimal(Sorgu.SonucToplamKusur);
                AnalizSonucuCikanUrunBilgileri analiz = new AnalizSonucuCikanUrunBilgileri();
                analiz.ISINKODU = Sorgu.SonucUrunISINKODU;
                analiz.UrunAdi = Sorgu.SonucUrunAdi;
                analiz.UrunKodu = Sorgu.SonucUrunKodu;
                return View(Tuple.Create(ak, Ans,analiz, AnalizGenelBilgileri));
            }
            else
            {
                AnalizKalemleri ak = new AnalizKalemleri();
                ak.AnalizListesiId = id;
                AnalizSonuclari Ans = new AnalizSonuclari();
                AnalizSonucuCikanUrunBilgileri analizUrun = new AnalizSonucuCikanUrunBilgileri();
                return View(Tuple.Create(ak, Ans,analizUrun, AnalizGenelBilgileri));
            }
            

        }
        [Authorize(Roles = "LaboratuvarAnalizListesi")]
        [HttpPost]
        public ActionResult LaboratuvarKayisiAnalizi(AnalizKalemleri ak)
        {
            try
            {
                string SubeKodu = Session["KullaniciSubeKodu"].ToString();
                LidosSistemiEntities db = new LidosSistemiEntities();
                int id = ak.AnalizListesiId;
                var AnalizGenelBilgileri = db.LabLaboratuvarAnalizListesi.Where(t => t.SubeKodu == SubeKodu && t.LabAnalizId == id).FirstOrDefault();
                AnalizSonuclari analizSonuc = analizSonuclari(ak);
                AnalizSonuclari analizSonuc2 = analizSonuc;
                AnalizKalemleri Anka = ak;
                AnalizSonucuCikanUrunBilgileri analizUrun = AnalizSonucuCikanUrun(analizSonuc2);
                return View(Tuple.Create(Anka, analizSonuc,analizUrun, AnalizGenelBilgileri));
            }
            catch (Exception e)
            {
                string mesaj = e.Message;
                throw;
            }
            


        }

        [Authorize(Roles = "LaboratuvarAnalizListesi")]
        public ActionResult LaboratuvarKayisiAnalizKaydet(AnalizKalemleri ak)
        {
            try
            {
                string SubeKodu = Session["KullaniciSubeKodu"].ToString();
                string KullaniciAdi = Session["KullaniciAdi"].ToString();
                LidosSistemiEntities db = new LidosSistemiEntities();
                int id = ak.AnalizListesiId;             
                AnalizSonuclari analizSonuc = analizSonuclari(ak);
                AnalizSonuclari analizSonuc2 = analizSonuc;
             
                AnalizSonucuCikanUrunBilgileri analizUrun = AnalizSonucuCikanUrun(analizSonuc2);
                var AnalizGenelBilgileri = db.LabLaboratuvarAnalizListesi.Where(t => t.SubeKodu == SubeKodu && t.LabAnalizId == id).FirstOrDefault();
                if (analizSonuc.RedEdilmeNedeni=="Hayır")
                {
                    AnalizGenelBilgileri.Durumu = "Tamamlandı";
                }
                else
                {
                    AnalizGenelBilgileri.Durumu = "RedEdildi";
                }
               
                db.SaveChanges();
                var AnalizKaydiVarmi = db.LabKayisiAnalizDetaylari.Where(t => t.SubeKodu == SubeKodu && t.AnalizListeId == id).FirstOrDefault();
                if (AnalizKaydiVarmi!=null)
                {
                    LabKayisiAnalizDetaylari dt = db.LabKayisiAnalizDetaylari.Where(t => t.SubeKodu == SubeKodu && t.AnalizListeId == id).FirstOrDefault(); 
                    dt.AgirKirliTane = ak.AgirKirliTane.ToString();
                    dt.AnalizListeId = id;
                    dt.BocekHasarli = ak.BocekHasarli.ToString();
                    dt.CekirdekliKayisi = ak.CekirdekliKayisi.ToString();
                    dt.CilliTane = ak.CilliTane.ToString();
                    dt.DoluYaraliTane = ak.DoluYaraliTane.ToString();
                    dt.GunesYanigi = ak.GunesYanigi.ToString();
                    dt.HafifKirliTane = ak.HafifKirliTane.ToString();
                    dt.HasarliTane = ak.HasarliTane.ToString();
                    dt.KaydedenUser = KullaniciAdi;
                    dt.KayitZamani = DateTime.Now;
                    dt.KukurtOrani = ak.KukurtOrani.ToString();
                    dt.OluBocekParcalari = ak.OluBocekParcalari.ToString();
                    dt.RenkUyumsuzlugu = ak.RenkUyumsuzlugu.ToString();
                    dt.RutubetOrani = ak.RutubetOrani.ToString();
                    dt.TaneAdeti = ak.TaneAdeti.ToString();
                    dt.YabanciMaddeBitkisel = ak.CekirdekliKayisi.ToString();
                    dt.YabanciMaddeTehlikeli = ak.CekirdekliKayisi.ToString();
                    dt.SonucBoyDerecesi = analizSonuc.BoyDerecesi.ToString();
                    dt.SonucGurupAdi = analizSonuc.GurupAdi.ToString();
                    dt.SonucKayisiKukurtTipi = analizSonuc.KayisiKukurtTipi.ToString();
                    dt.SonucRedEdilmeAciklamasi = analizSonuc.RedEdilmeNedeni.ToString();
                    if (analizSonuc.RedEdilmeNedeni=="Hayır")
                    {
                        dt.SonucRedVarmi = "Hayır";
                    }
                    else
                    {
                        dt.SonucRedVarmi = "Evet";
                    }
                    
                    dt.SonucToplamKusur = analizSonuc.ToplamKusur.ToString();
                    dt.SonucUrunAdi = analizUrun.UrunAdi;
                    dt.SonucUrunISINKODU = analizUrun.ISINKODU;
                    dt.SonucUrunKodu = analizUrun.UrunKodu;
                    dt.SubeKodu = SubeKodu;
                    db.SaveChanges();
                    int KayitNo = Convert.ToInt32(AnalizGenelBilgileri.KantarKayitNo);
                    var MudiBilgileri = db.KantarTartimKayitlari.Where(t => t.KantarKayitNo == KayitNo && t.KaydedenSube == SubeKodu).FirstOrDefault();
                    
                    EksperKayisiDepoOnKayit dok = db.EksperKayisiDepoOnKayit.Where(t=>t.SubeKodu==SubeKodu&&t.AnalizListeId==id).FirstOrDefault();
                    dok.AnalizListeId = id;
                    dok.DepoAdi = "Depo Seçilmedi(G.Y)";
                    dok.KantarKayitId = MudiBilgileri.KantarKayitId;
                    dok.KaydedenUser = KullaniciAdi;
                    dok.KayitTarihi = DateTime.Now;
                    dok.NumuneKayitNo = AnalizGenelBilgileri.NumuneNo;
                    dok.RafAdi = "Raf Seçilmedi(G.Y)";
                    dok.SubeKodu = SubeKodu;
                    dok.Durumu = "Laboratuvardan Güncelleme Yapıldı";
                    db.SaveChanges();
                }
                else
                {
                    LabKayisiAnalizDetaylari dt = new LabKayisiAnalizDetaylari();
                    dt.AgirKirliTane = ak.AgirKirliTane.ToString();
                    dt.AnalizListeId = id;
                    dt.BocekHasarli = ak.BocekHasarli.ToString();
                    dt.CekirdekliKayisi = ak.CekirdekliKayisi.ToString();
                    dt.CilliTane = ak.CilliTane.ToString();
                    dt.DoluYaraliTane = ak.DoluYaraliTane.ToString();
                    dt.GunesYanigi = ak.GunesYanigi.ToString();
                    dt.HafifKirliTane = ak.HafifKirliTane.ToString();
                    dt.HasarliTane = ak.HasarliTane.ToString();
                    dt.KaydedenUser = KullaniciAdi;
                    dt.KayitZamani = DateTime.Now;
                    dt.KukurtOrani = ak.KukurtOrani.ToString();
                    dt.OluBocekParcalari = ak.OluBocekParcalari.ToString();
                    dt.RenkUyumsuzlugu = ak.RenkUyumsuzlugu.ToString();
                    dt.RutubetOrani = ak.RutubetOrani.ToString();
                    dt.TaneAdeti = ak.TaneAdeti.ToString();
                    dt.YabanciMaddeBitkisel = ak.CekirdekliKayisi.ToString();
                    dt.YabanciMaddeTehlikeli = ak.CekirdekliKayisi.ToString();
                    dt.SonucBoyDerecesi = analizSonuc.BoyDerecesi.ToString();
                    dt.SonucGurupAdi = analizSonuc.GurupAdi.ToString();
                    dt.SonucKayisiKukurtTipi = analizSonuc.KayisiKukurtTipi.ToString();
                    dt.SonucRedEdilmeAciklamasi = analizSonuc.RedEdilmeNedeni.ToString();
                    if (analizSonuc.RedEdilmeNedeni == "Hayır")
                    {
                        dt.SonucRedVarmi = "Hayır";
                    }
                    else
                    {
                        dt.SonucRedVarmi = "Evet";
                    }
                    dt.SonucToplamKusur = analizSonuc.ToplamKusur.ToString();
                    dt.SonucUrunAdi = analizUrun.UrunAdi;
                    dt.SonucUrunISINKODU = analizUrun.ISINKODU;
                    dt.SonucUrunKodu = analizUrun.UrunKodu;
                    dt.SubeKodu = SubeKodu;

                    db.LabKayisiAnalizDetaylari.Add(dt);
                    db.SaveChanges();

                    int KayitNo = Convert.ToInt32(AnalizGenelBilgileri.KantarKayitNo);
                    var MudiBilgileri = db.KantarTartimKayitlari.Where(t => t.KantarKayitNo == KayitNo && t.KaydedenSube == SubeKodu).FirstOrDefault();

                    EksperKayisiDepoOnKayit dok = new EksperKayisiDepoOnKayit();
                    dok.AnalizListeId = id;
                    dok.DepoAdi = "Depo Seçilmedi";
                    dok.KantarKayitId = MudiBilgileri.KantarKayitId;
                    dok.KaydedenUser = KullaniciAdi;
                    dok.KayitTarihi = DateTime.Now;
                    dok.NumuneKayitNo = AnalizGenelBilgileri.NumuneNo;
                    dok.RafAdi = "Raf Seçilmedi";
                    dok.SubeKodu = SubeKodu;
                    dok.Durumu = "Laboratuvardan Ekleme Yapıldı";
                    db.EksperKayisiDepoOnKayit.Add(dok);
                    db.SaveChanges();

                }

                return RedirectToAction("LaboratuvarAnalizListesi");
            }
            catch (Exception e)
            {
                string mesaj = e.Message;
                throw;
            }



        }

        [Authorize(Roles = "LaboratuvarAnalizListesi")]
    
        public ActionResult LaboratuvarKayisiAnaliziYazdir(int id)
        {
            try
            {
                string SubeKodu = Session["KullaniciSubeKodu"].ToString();
                LidosSistemiEntities db = new LidosSistemiEntities();
                var KayitBul = db.LabKayisiAnalizDetaylari.Where(t => t.AnalizListeId == id && t.SubeKodu == SubeKodu).FirstOrDefault();
                return View(KayitBul);
            }
            catch (Exception e)
            {
                string mesaj = e.Message;
                throw;
            }
        }
    }
}