﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using YeniOtomasyonSistemi.Models;
using YeniOtomasyonSistemi.Models.Login;

namespace YeniOtomasyonSistemi.Controllers
{
    public class SecurityController : Controller
    {
        // GET: Security
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(LKullanicilar kullanicilar)
        {
            LidosSistemiEntities db = new LidosSistemiEntities();
            if (new LoginState().IsLoginSuccess(kullanicilar.KullaniciAdi, kullanicilar.KullaniciSifresi))
            {
                FormsAuthentication.SetAuthCookie(Convert.ToString(Session["KullaniciId"]), false);
                return RedirectToAction("Index", "Home");
            }
            TempData["message"] = "Kullanıcı adınız veya şifreniz yanlış!";
            return RedirectToAction("Login");
        }

        public ActionResult Logout()
        {
            Session.Clear();
            FormsAuthentication.SignOut();
            Response.Cookies[".ASPXAUTH"].Expires = DateTime.Now.AddDays(-1);
            return RedirectToAction("Login");
        }
    }
}