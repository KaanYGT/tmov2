﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using YeniOtomasyonSistemi.Models;

namespace YeniOtomasyonSistemi.Security
{
    public class KullaniciMenuErisimIzinleri : RoleProvider
    {
        public override string ApplicationName 
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); } 
        }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            throw new NotImplementedException();
        }

        public override string[] GetRolesForUser(string username)
        {
            int KullanciId = Convert.ToInt32(username);
            LidosSistemiEntities db = new LidosSistemiEntities();
            var AltMenuListesi = db.LKullaniciMenuler.Where(t => t.KullaniciId == KullanciId).ToList();
            string AltMenuIzinListesi = "";
            foreach (var item in AltMenuListesi)
            {
                AltMenuIzinListesi = AltMenuIzinListesi +","+ item.LMenuAlt.ActionAdi;
            }
            string[] ArrayListesi = new string[]
            {
                ""
            };
            ArrayListesi = AltMenuIzinListesi.Split(',');
            return ArrayListesi;
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            throw new NotImplementedException();
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }
    }
}